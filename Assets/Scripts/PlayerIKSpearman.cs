﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class PlayerIKSpearman : PlayerIK
    {
        [SerializeField]
        Transform arm;
        [SerializeField]
        float maxYShift = 1.3f;
        [SerializeField]
        float armSpeed = 0.05f;
        float armStartY;

        public override void Start()
        {
            base.Start();
            armStartY = arm.localPosition.y;
        }

        protected override void OnSwipe(float x, float y)
        {
            base.OnSwipe(x, y);
            if (!isActive || !isAiming || isFrozen)
                return;

            float diff = Mathf.Clamp(arm.localPosition.y - x * armSpeed, armStartY, armStartY + maxYShift);
            arm.localPosition = new Vector3(arm.localPosition.x, diff, arm.localPosition.z);
            currentForce = Mathf.Lerp(minForce, maxForce, (new Vector3(arm.localPosition.x, armStartY, arm.localPosition.z) - arm.localPosition).magnitude / maxYShift);
        }

        protected override void Fire()
        {
            base.Fire();
            arm.localPosition = new Vector3(arm.localPosition.x, armStartY, arm.localPosition.z);
        }
    }
}
