﻿using System.Linq;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

namespace Stickman
{
    public class Game : MonoBehaviour
    {
        public static Game Instance;

        public static System.Action OnLose = delegate { };
        public static System.Action<int, int> OnBotDie = delegate { };

        [SerializeField]
        GameObject[] castlesPrefabs;
        [SerializeField]
        GameObject[] playerPrefabs;
        [SerializeField]
        GameObject[] botPrefabs;
        [SerializeField]
        GameObject botEffectPrefab;
        [SerializeField]
        float spawnInterval = 1f;

        [SerializeField]
        GameObject bonusPrefab;

        [SerializeField]
        GameObject batPrefab;
        [SerializeField]
        GameObject dragonPrefab;
        [SerializeField][Range(20f, 40f)]
        float dragonSpawnInterval = 20f;
        float dragonSpawnTime;
        [SerializeField]
        int dragonSpawnCountAgressive = 4;
        int dragonSpawnCount = 0;

        [SerializeField]
        GameObject backgroundPrefab;
        Background background;

        List<Transform> playerPoints = new List<Transform>();
        List<Transform> enemyPoints = new List<Transform>();

        GameObject castle;

        PlayerBase player;
        List<GameObject> bots = new List<GameObject>();
        Dictionary<GameObject, Transform> botsByPoints = new Dictionary<GameObject, Transform>();
        List<Transform> buisyPoints = new List<Transform>();
        int maxBotCount = 1;
        float botCountCheckInterval = 7f;
        float botCountCheckTime = 0f;

        int botKilled = 0;
        float gameTime = 0f;
        bool isStarted = false;
        bool isTutor = false;

        void Awake()
        {
            Instance = this;
            PlayerBase.OnDie += OnPlayerDie;
            Background.OnChangeDayPart += OnChangeDayPart;
        }

        void OnDestroy()
        {
            PlayerBase.OnDie -= OnPlayerDie;
            Background.OnChangeDayPart -= OnChangeDayPart;
        }

        public int GetScores()
        {
            return botKilled;
        }

        void OnChangeDayPart(Background.DayPart dayPart)
        {
            if (dayPart == Background.DayPart.Night && !isTutor)
                Invoke("SpawnBat", Random.Range(2f, 4f));
        }

        void OnPlayerDie(PlayerBase player)
        {
            if (!isStarted)
                return;
            if (player.IsBot())
            {
                ++botKilled;
                int bonus = player.GetLevel() + 5 + PlayerPrefs.GetInt("castleIndex", 0) * 15;
                SpawnBonus(player.transform.position + Vector3.up * 5f, bonus);
                OnBotDie(botKilled, bonus);
                bots.Remove(player.gameObject);
                botsByPoints.Remove(player.gameObject);
                if (bots.Count == 0)
                    StartCoroutine(SpawnBot());
                SetMaxBotCount();
            }
            else
            {
                int maxScores = PlayerPrefs.GetInt("maxScores", 0);
                if (botKilled > maxScores)
                    PlayerPrefs.SetInt("maxScores", botKilled);
                
                isStarted = false;
                StopAllCoroutines();
                OnLose();
            }
        }

        void Update()
        {
            if (!isStarted || isTutor)
                return;

            dragonSpawnTime -= Time.deltaTime;
            if (dragonSpawnTime < 0)
            {
                SpawnDragon();
                dragonSpawnTime = dragonSpawnInterval + Random.Range(0f, 15f);
            }

            botCountCheckTime += Time.deltaTime;
            gameTime += Time.deltaTime;
            if (botCountCheckTime > botCountCheckInterval)
            {
                botCountCheckTime = 0f;

                if (bots.Count > 0 && bots.Count < maxBotCount)
                {
                    StartCoroutine(SpawnBot());
                }
            }
        }

        IEnumerator SpawnBot()
        {
            List<Transform> freePoints = new List<Transform>(enemyPoints);
            freePoints.RemoveAll(x => botsByPoints.Values.Contains(x));
            if (buisyPoints.Count > 0)
                freePoints.RemoveAll(x => buisyPoints.Contains(x));
            if (freePoints.Count == 0)
            {
                Debug.LogWarning("No free spawn points!");
                yield break;
            }
            Transform pt = freePoints[Random.Range(0, freePoints.Count)];
            buisyPoints.Add(pt);
            yield return new WaitForSeconds(spawnInterval);
            buisyPoints.Remove(pt);
            int level = GetBotLevel();
            BotEquip equip = GameVariables.botEquipByLevel[level][Random.Range(0, GameVariables.botEquipByLevel[level].Count)];
            var botEffect = Instantiate(botEffectPrefab, transform);
            botEffect.transform.position = pt.position;
            GameSoundController.instance.PlaySound("PortalOpen");
            yield return new WaitForSeconds(0.5f);
            var bot = Instantiate(botPrefabs[(int)equip.weapon], transform);
            bot.transform.position = pt.position;
            bot.GetComponent<BotIK>().Generate(equip);
            botsByPoints.Add(bot, pt);
            bots.Add(bot);
            Destroy(botEffect, 1f);
        }

        int GetBotLevel()
        {
            int level = 0;
            int playerLevel = player.GetLevel();
            int addLevel = 0;
            if (botKilled > 7)
                addLevel = 1;
            if (botKilled > 15)
                addLevel = 2;
            if (botKilled > 30)
                addLevel = 3;
            int minLevel = Mathf.Max(playerLevel - 5 + addLevel, 0);
            int maxLevel = Mathf.Min(playerLevel + addLevel, GameVariables.botEquipByLevel.Count);
            level = Random.Range(minLevel, maxLevel);

            return level;
        }

        void SetMaxBotCount()
        {
            if (maxBotCount < 2 && gameTime > 90f && botKilled >= 13)
                maxBotCount += 1;
            if (maxBotCount < 3 && gameTime > 180f && botKilled >= 25)
                maxBotCount += 1;
        }

        void SpawnBat()
        {
            Instantiate(batPrefab, transform);
        }

        void SpawnDragon()
        {
            if (PlayerPrefs.GetInt("dragonIsDead", 0) == 1)
                return;

            var go = Instantiate(dragonPrefab, transform);
            var dragon = go.GetComponent<Dragon>();
            dragon.SetAgressive(dragonSpawnCount >= dragonSpawnCountAgressive);
            dragonSpawnCount++;
        }

        public void SpawnBonus(Vector3 pos, int amount)
        {
            var bonus = Instantiate(bonusPrefab, transform);
            bonus.transform.position = pos;
            bonus.transform.DOLocalMoveY(bonus.transform.position.y + 7f, 1.5f);
            bonus.GetComponent<SpriteRenderer>().DOFade(0, 1.5f).SetEase(Ease.InQuad);
            var txt = bonus.GetComponentInChildren<TMPro.TextMeshPro>();
            txt.text = amount.ToString();
            txt.DOFade(0, 1.5f).SetEase(Ease.InQuad);
            Destroy(bonus, 1f);
        }

        public void Restart()
        {
            Stop();
            isTutor = PlayerPrefs.GetInt("TutorShown", 0) == 0;
            isStarted = true;
            int castleId = PlayerPrefs.GetInt("castleIndex", 0);
            maxBotCount = 1 + PlayerPrefs.GetInt("castleIndex", 0);
            var bo = Instantiate(backgroundPrefab, transform);
            background = bo.GetComponent<Background>();
            background.SetDayPart(castleId == 0 ? Background.DayPart.Day : (Random.value > 0.5f ? Background.DayPart.Evening : Background.DayPart.Night));
            castle = Instantiate(castlesPrefabs[castleId], transform);

            playerPoints = GameObject.FindGameObjectsWithTag("PlayerPoint").Select(x => x.transform).ToList();
            enemyPoints = GameObject.FindGameObjectsWithTag("EnemyPoint").Select(x => x.transform).ToList();

            if (playerPoints.Count == 0)
                Debug.LogError("Too few player spawn points!");
            if (enemyPoints.Count < 2)
                Debug.LogError("Too few bot spawn points!");


            Transform pt = playerPoints[Random.Range(0, playerPoints.Count)];
            int weaponIndex = PlayerPrefs.GetInt("playerWeaponIndex", 0);
            player = Instantiate(playerPrefabs[weaponIndex]).GetComponent<PlayerBase>();
            player.transform.position = pt.position;
            botKilled = 0;
            dragonSpawnCount = 0;
            PlayerPrefs.SetInt("dragonIsDead", 0);
            PlayerPrefs.SetInt("dragonHealth", GameVariables.dragonMaxLife + Dragon.GetAdditionalLife());

            if (!isTutor)
            {
                StartSpawn();
            }
        }

        void StartSpawn()
        {
            StartCoroutine(SpawnBot());

            if (PlayerPrefs.GetInt("isFirstStart", 1) == 1)
            {
                PlayerPrefs.SetInt("isFirstStart", 0);
                dragonSpawnTime = Random.Range(2f, 5f);
            }
            else
            {
                dragonSpawnTime = dragonSpawnInterval + Random.Range(3f, 15f);
            }

        }

        public void Stop()
        {
            StopAllCoroutines();
            isStarted = false;
            gameTime = 0f;
            if (castle != null)
                DestroyImmediate(castle);
            if (background != null)
                Destroy(background.gameObject);

            playerPoints.Clear();
            enemyPoints.Clear();
            buisyPoints.Clear();

            foreach (var bot in bots)
            {
                Destroy(bot);
            }
            bots.Clear();
            botsByPoints.Clear();

            if (player != null)
                Destroy(player.gameObject);

            var bolts = FindObjectsOfType<Bolt>();
            foreach (var it in bolts)
                Destroy(it.gameObject);

            for (int i = 0; i < transform.childCount; ++i)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

        public void OnTutorComplete()
        {
            isTutor = false;
            StartSpawn();
        }

        public bool HealPlayer()
        {
            if (!isStarted || player == null || player.IsDead() || player.IsFullHealth())
                return false;

            player.Heal();
            return true; 
        }

        public void ChangeArrow()
        {
            if (player && player.HasBolt())
                player.CreateBolt();
        }
    }
}
