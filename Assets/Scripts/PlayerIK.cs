﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class PlayerIK : PlayerBase
    {
        public override void Awake()
        {
            base.Awake();

            SwipeController.OnSwipe += OnSwipe;
            SwipeController.OnStart += OnSwipeStart;
            SwipeController.OnRelease += OnSwipeRelease;
        }

        public override void Start()
        {
            base.Start();

            int headArmorId = PlayerPrefs.GetInt("playerHeadArmodIndex", -1);
            int bodyArmorId = PlayerPrefs.GetInt("playerBodyArmodIndex", -1);
            armorController.SetArmors(new Dictionary<BodyPart, int>() { { BodyPart.Head, headArmorId }, { BodyPart.BodyPart, bodyArmorId } });
            SetShield((ShieldType)PlayerPrefs.GetInt("playerShieldIndex", 0));
        }

        void OnDestroy()
        {
            SwipeController.OnSwipe -= OnSwipe;
            SwipeController.OnStart -= OnSwipeStart;
            SwipeController.OnRelease -= OnSwipeRelease;
        }

        protected virtual void OnSwipeStart(float x, float y)
        {
            if (isFrozen)
                return;

            if (currentBolt != null)
                isAiming = true;
        }

        protected virtual void OnSwipeRelease(float x, float y)
        {
            if (isFrozen)
                isAiming = false;

            if (!isAiming)
                return;

            Fire();
        }

        protected virtual void OnSwipe(float x, float y)
        {
            if (!isActive || !isAiming || isFrozen)
                return;

            currentForce = maxForce;
            aim.localPosition = new Vector3(aim.localPosition.x, Mathf.Clamp(aim.localPosition.y + y * aimSpeed, bottompAimOffset, topAimOffset), aim.localPosition.z);
        }
    }
}
