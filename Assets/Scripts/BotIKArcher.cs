﻿using UnityEngine;

using DG.Tweening;

namespace Stickman
{
    public class BotIKArcher : BotIK
    {
        [SerializeField]
        Transform arm;
        [SerializeField]
        float maxXShift = 1.3f;
        float armStartX;

        [SerializeField]
        LineRenderer bowSpring;
        [SerializeField]
        Transform upperPoint;
        [SerializeField]
        Transform downPoint;

        public override void Start()
        {
            base.Start();

            if (player != null)
                aim.position = new Vector3(player.GetHead().position.x, aim.position.y, aim.position.z);
            armStartX = arm.localPosition.x;
        }

        public override void CreateBolt()
        {
            base.CreateBolt();
            if (isFrozen)
                return;
            aim.DOMoveY(currentAimOffset, currAimingTime - cooldown);
            arm.DOLocalMoveX(arm.localPosition.x - Mathf.Lerp(0, maxXShift, currentForce / maxForce), currAimingTime - cooldown);
        }

        protected override void Update()
        {
            base.Update();

            if (bowSpring != null)
            {
                bowSpring.SetPosition(0, upperPoint.position);
                bowSpring.SetPosition(1, arm.position);
                bowSpring.SetPosition(2, downPoint.position);
            }
        }

        protected override void Fire()
        {
            base.Fire();
            arm.DOComplete();
            arm.localPosition = new Vector3(armStartX, arm.localPosition.y, arm.localPosition.z);
        }

        public override void Die()
        {
            base.Die();
            if (bowSpring)
                Destroy(bowSpring.gameObject);
        }

        public override void Freeze(bool val)
        {
            base.Freeze(val);

            if (val)
            {
                aim.DOComplete();
                arm.DOComplete();
            }
            else
            {
                arm.localPosition = new Vector3(armStartX, arm.localPosition.y, arm.localPosition.z);
                CreateBolt();
            }
        }
    }
}
