﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class Bolt : MonoBehaviour
    {
        [SerializeField]
        float destroyDelay;

        [HideInInspector]
        public float force = 1f;
        Rigidbody2D body;
        bool isFired = false;

        BoltType boltType;

        [SerializeField]
        GameObject[] heads;

        GameObject holder;
        Quaternion currRotation;

        IDamageble target;
        BodyPart bodyPart = BodyPart.BodyPart;

        int damage = 1;

        void Start()
        {
            body = GetComponent<Rigidbody2D>();
            body.bodyType = RigidbodyType2D.Kinematic;
        }

        public void Init(BoltType boltType, int damage)
        {
            this.damage = damage;
            this.boltType = boltType;
            int headIndex = (int)boltType;
            if (headIndex > heads.Length - 1)
                headIndex = 0;
            foreach (var it in heads)
                it.SetActive(false);
            heads[headIndex].SetActive(true);
        }

        public void Fire()
        {
            isFired = true;
            body.bodyType = RigidbodyType2D.Dynamic;
            body.AddForce(transform.right * force);
            Destroy(gameObject, 30f);
        }

        void Update()
        {
            if (!isFired)
                return;

            if (body.velocity.magnitude <= float.Epsilon)
                return;

            var dir = body.velocity;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            currRotation = transform.rotation;
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            if (!isFired)
                return;

            isFired = false;
            Transform prnt = coll.transform;
            transform.rotation = currRotation;
            holder = new GameObject();
            holder.transform.parent = coll.transform.transform;
            transform.parent = holder.transform;
            Destroy(body);
            GetComponent<Collider2D>().enabled = false;
            var tr = GetComponentInChildren<TrailRenderer>();
            if (tr)
                tr.enabled = false;
            Destroy(gameObject, destroyDelay);
            Destroy(holder, destroyDelay);
            target = coll.gameObject.GetComponent<IDamageble>();
            if (target == null)
            {
                target = coll.gameObject.GetComponentInParent<IDamageble>();
            }
            if (target != null)
            {
                if (PlayerBase.bodyTags.ContainsKey(coll.tag))
                    bodyPart = PlayerBase.bodyTags[coll.tag];
                else
                    bodyPart = PlayerBase.bodyTags[BodyPart.BodyPart.ToString()];
                MakeDamage();
            }
        }

        void MakeDamage()
        {
            if (target == null)
                return;

            target.TakeDamage(damage, bodyPart, boltType);
        }
    }
}
