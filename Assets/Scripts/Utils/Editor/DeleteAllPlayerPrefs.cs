﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Stickman
{
    public class DeleteAllPlayerPrefs : MonoBehaviour
    {
#if UNITY_EDITOR
        [MenuItem("Tools/Delete All PlayerPrefs")]
        static public void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
        }
#endif
    }
}
