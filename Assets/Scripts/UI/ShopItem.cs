﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Stickman
{
    public class ShopItem : MonoBehaviour
    {
        public int price;

        [SerializeField]
        Text priceText;

        [SerializeField]
        protected Button buyButton;

        protected virtual void Start()
        {
            priceText.text = price.ToString();
        }

        public virtual void Refresh()
        {
            int crystals = PlayerPrefs.GetInt("crystals", 0);
            buyButton.interactable = crystals >= price;
        }
    }
}
