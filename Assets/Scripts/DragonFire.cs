﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class DragonFire : MonoBehaviour
    {
        [SerializeField]
        int damage = 1000;

        void OnTriggerEnter2D(Collider2D coll)
        {
            var damageble = coll.GetComponentInParent<IDamageble>();
            if (damageble == null)
                return;

            if (damageble is Dragon)
                return;

            damageble.TakeDamage(damage);
        }
    }
}
