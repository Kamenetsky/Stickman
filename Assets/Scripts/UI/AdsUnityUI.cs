﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;


namespace Stickman
{
    public class AdsUnityUI : MonoBehaviour
    {
        [SerializeField]
        Button[] rewardButtons;

        [SerializeField]
        Text[] amountTexts;

        [SerializeField]
        int rewardAmount = 50;

        string gameId = "2790569";

        [SerializeField]
        ShopUI shop;

        [SerializeField]
        GameUI gameUI;

        bool isInited = false;

        void Init()
        {
            if (isInited)
                return;

            isInited = true;

            Debug.Log("!!! Ads inited");
            
            foreach (var it in amountTexts)
            {
                it.text = "x" + rewardAmount.ToString();
            }

            bool adsEnabled = true;
            if (!Advertisement.isSupported)
            {
                adsEnabled = false;
                Debug.Log("Ads not supported");
            }

            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                adsEnabled = false;
                Debug.Log("No connection");
            }

            if (!adsEnabled)
            {
                foreach (var it in rewardButtons)
                {
                    it.interactable = false;
                }
                return;
            }
            Advertisement.Initialize(gameId, false);
        }

        void Awake()
        {
            Init();
            foreach (var it in rewardButtons)
            {
                var listener = it.gameObject.GetStateListener();
                listener.OnEnabled += Init;
                listener.OnDisabled += () => isInited = false;
            }
        }

        public void OnShowReward()
        {
            if (!Advertisement.IsReady())
                return;

            ShowOptions options = new ShowOptions();
            options.resultCallback = RewardShownHandle;
            
            Advertisement.Show("rewardedVideo", options);
        }

        void RewardShownHandle(ShowResult result)
        {
            if (result == ShowResult.Finished)
            {
                gameUI.AddCrystals(rewardAmount);
                shop.Refresh();
            }
        }
    }
}
