﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Stickman
{
    public class ShopItemArmor : ShopItem
    {
        public ArmorType armorType;

        public string prefix = "Head";

        [SerializeField]
        Button equipButton;

        [SerializeField]
        GameObject equipedIcon;

        protected override void Start()
        {
            base.Start();
        }

        public override void Refresh()
        {
            base.Refresh();

            int playerArmorIndex = PlayerPrefs.GetInt("player" + prefix + "ArmodIndex", -1);
            bool isArmorBouth = PlayerPrefs.GetInt("isArmorBouth" + prefix + armorType.ToString(), 0) == 1;
            bool isArmorEquiped = playerArmorIndex == (int)armorType - 1;
            buyButton.gameObject.SetActive(!isArmorBouth);
            equipButton.gameObject.SetActive(isArmorBouth && !isArmorEquiped);
            equipedIcon.SetActive(isArmorEquiped);
        }
    }
}
