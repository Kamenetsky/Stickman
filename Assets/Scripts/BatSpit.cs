﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class BatSpit : MonoBehaviour
    {
        [SerializeField]
        ParticleSystem splash;

        [SerializeField]
        GameObject trail;

        [SerializeField]
        float speed = 1f;

        SpriteRenderer body;

        bool isMoving = true;

        void Start()
        {
            body = GetComponent<SpriteRenderer>();
            splash.gameObject.SetActive(false);
        }

        void Update()
        {
            if (!isMoving)
                return;

            transform.position = transform.position - transform.up * speed * Time.deltaTime;
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            if (!isMoving)
                return;

            trail.SetActive(false);
            body.color = new Color(0f, 0f, 0f, 0f);
            isMoving = false;
            var target = coll.gameObject.GetComponent<IDamageble>();
            if (target == null)
            {
                target = coll.gameObject.GetComponentInParent<IDamageble>();
            }
            if (target != null)
            {
                target.TakeDamage(1);
            }
            splash.gameObject.SetActive(true);
            splash.Play();
            GameSoundController.instance.PlaySound("BatSpitImpact");
            Destroy(gameObject, 1f);
        }
    }
}
