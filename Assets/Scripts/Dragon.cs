﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Stickman
{
    public class Dragon : MonoBehaviour, IDamageble
    {
        public static System.Action OnDie = delegate { };

        [SerializeField]
        Animator animator;

        [SerializeField]
        ParticleSystem fire;

        [SerializeField]
        GameObject fireDamage;

        [SerializeField]
        float fireTime = 5f;

        [SerializeField]
        float speed = 20f;

        [SerializeField]
        Transform head;

        [SerializeField]
        Transform neck;

        [SerializeField]
        Transform neck1;

        [SerializeField]
        UnityEngine.UI.Slider lifeProgress;

        [SerializeField]
        Color freezeColor;
        Color initColor;

        Rigidbody2D[] rigidbodies;
        HingeJoint2D[] joints;
        Collider2D[] colliders;
        SpriteRenderer[] spriteWings;
        SpriteRenderer spriteBody;

        bool isDead = false;
        bool isAgressive = false;
        int life;
        int initLife = 15;

        float freezeMultiplier = 1;
        float freezeTime = 0f;
        bool isFrozen = false;

        [SerializeField]
        int prize = 450;
        
        void Awake()
        {
            rigidbodies = GetComponentsInChildren<Rigidbody2D>();
            joints = GetComponentsInChildren<HingeJoint2D>();
            colliders = GetComponentsInChildren<Collider2D>();
            spriteWings = GetComponentsInChildren<SpriteRenderer>();
            spriteBody = GetComponent<SpriteRenderer>();
            initColor = spriteBody.color;
            fire.gameObject.SetActive(false);
            fireDamage.SetActive(false);
            
            foreach (var it in rigidbodies)
            {
                it.isKinematic = true;
            }

            foreach (var it in joints)
            {
                it.enabled = false;
            }

            life = PlayerPrefs.GetInt("dragonHealth");
            initLife = GameVariables.dragonMaxLife + GetAdditionalLife();
            SetHealth(true);
        }

        public static int GetAdditionalLife()
        {
            int weaponIndex = PlayerPrefs.GetInt("playerWeaponIndex", 0);
            int addDragonLife = 0;
            if (weaponIndex > 1)
                addDragonLife = 3;
            if (weaponIndex > 2)
                addDragonLife = 5;

            return addDragonLife;
        }

        public void SetAgressive(bool value)
        {
            isAgressive = value;
            if (isAgressive)
                Invoke("Fire", fireTime);
        }

        void SetHealth(bool instant = false)
        {
            PlayerPrefs.SetInt("dragonHealth", life);
            if (lifeProgress == null)
                return;

            lifeProgress.DOValue(((float)life) / ((float)initLife), 0.3f);
            if (instant)
                lifeProgress.value = ((float)life) / ((float)initLife);
        }

        void Fire()
        {
            fire.gameObject.SetActive(true);
            fireDamage.SetActive(true);
            GameSoundController.instance.PlaySound("Dragon");
            GameSoundController.instance.PlaySound("DragonFlame");
        }

        void Update()
        {
            if (isDead)
            {
                if (speed > 0f)
                    speed -= 2 * Time.deltaTime;
            }

            if (isFrozen)
            {
                freezeTime -= Time.deltaTime;
                if (freezeTime < 0f)
                    Freeze(false);
            }
                
            transform.Translate(Vector3.left * speed * freezeMultiplier * Time.deltaTime);

            if (transform.position.x < -60f)
                Destroy(gameObject);
        }
        
        public void TakeDamage(int damage)
        {
            life -= damage;
            SetHealth();
            GameSoundController.instance.PlaySound("ArrowHitBody");
            if (life <= 0)
                Die();
        }

        public void TakeDamage(int damage, BodyPart bodyPart, BoltType boltType)
        {
            life -= damage;
            SetHealth();
            GameSoundController.instance.PlaySound("ArrowHitBody");
            if (life <= 0)
            {
                Die();
                return;
            }

            if (boltType == BoltType.Frost)
                Freeze(true);
        }

        void Freeze(bool isFreeze)
        {
            isFrozen = isFreeze;
            freezeTime = 2f;
            freezeMultiplier = isFreeze ? 0.7f : 1f;
            animator.speed = freezeMultiplier;
            spriteBody.color = isFreeze ? freezeColor : initColor;
            foreach (var it in spriteWings)
            {
                it.color = isFreeze ? freezeColor : initColor;
            }
        }

        void Die()
        {
            if (isDead)
                return;

            Game.Instance.SpawnBonus(transform.position + Vector3.up * 3f, prize);
            Game.OnBotDie(Game.Instance.GetScores(), prize);
            PlayerPrefs.SetInt("dragonIsDead", 1);
            OnDie();
            fire.Stop();
            fireDamage.SetActive(false);
            SetLayerRecursive(gameObject, 15);
            isDead = true;
            animator.enabled = false;
            lifeProgress.gameObject.SetActive(false);

            head.DORotate(head.rotation.eulerAngles + Vector3.back * 50f, 0.5f);
            neck.DORotate(neck.rotation.eulerAngles + Vector3.back * 50f, 0.5f);
            neck1.DORotate(neck1.rotation.eulerAngles + Vector3.back * 50f, 0.5f);

            foreach (var it in rigidbodies)
            {
                it.isKinematic = false;
            }

            foreach (var it in joints)
            {
                it.enabled = true;
                it.enableCollision = true;
            }

            foreach (var it in colliders)
            {
                it.isTrigger = false;
            }

            Invoke("FadeOff", 5f);

            Destroy(gameObject, 7);
        }

        void SetLayerRecursive(GameObject obj, int index)
        {
            obj.layer = index;
            for(int i = 0; i < obj.transform.childCount; ++i)
            {
                SetLayerRecursive(obj.transform.GetChild(i).gameObject, index);
            }
        }

        void FadeOff()
        {
            var renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach (var it in renderers)
            {
                it.DOFade(0f, 2f);
            }
        }
    }
}
