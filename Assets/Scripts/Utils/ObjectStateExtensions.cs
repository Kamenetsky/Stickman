﻿using System;
using UnityEngine;
 
public static class ObjectStateExtensions
{
    public static IStateListener GetStateListener(this GameObject obj)
    {
        return obj.GetComponent<ObjectStateListener>() ?? obj.AddComponent<ObjectStateListener>();
    }
 
    public interface IStateListener
    {
        event Action OnEnabled;
        event Action OnDisabled;
    }
 
    class ObjectStateListener : MonoBehaviour, IStateListener
    {
        public event Action OnEnabled;
        public event Action OnDisabled;
 
        void Awake()
        {
            hideFlags = HideFlags.DontSaveInBuild | HideFlags.HideInInspector;
        }
 
        void OnEnable()
        {
            TryInvoke(OnEnabled);
        }
 
        void OnDisable()
        {
            TryInvoke(OnDisabled);
        }
 
        void TryInvoke(Action action)
        {
            if (action != null)
                action();
        }
    }
}