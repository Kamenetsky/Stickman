﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class ArmorController : MonoBehaviour
    {
        [System.Serializable]
        public class ArmorInfo
        {
            public BodyPart bodyPart;
            [HideInInspector]
            public int totalArmor;
            [HideInInspector]
            public int currentArmor = 0;
            public List<GameObject> parts = new List<GameObject>();
        }

        [SerializeField]
        List<ArmorInfo> armors = new List<ArmorInfo>();
        Dictionary<BodyPart, int> armorIds = new Dictionary<BodyPart, int>();

        [SerializeField]
        float destructTime = 3f;

        [SerializeField]
        float force = 2000f;

        private void Awake()
        {
            foreach (BodyPart bp in System.Enum.GetValues(typeof(BodyPart)))
            {
                var arms = armors.FindAll(x => x.bodyPart == bp);
                for (int i = 0; i < arms.Count; ++i)
                {
                    arms[i].totalArmor = GameVariables.armorValues[i];
                    foreach (var part in arms[i].parts)
                        part.SetActive(false);
                }
            }
        }

        public void SetArmors(Dictionary<BodyPart, int> armorByPart)
        {
            armorIds.Clear();
            foreach (BodyPart bp in System.Enum.GetValues(typeof(BodyPart)))
            {
                int armorId = armorByPart[bp];
                if (armorId >= 0)
                {
                    var armor = armors.FindAll(x => x.bodyPart == bp)[armorId];
                    if (armor == null)
                    {
                        Debug.LogWarning("Can not find armor for part: " + bp.ToString() + " : " + armorId);
                        continue;
                    }
                    armorIds[bp] = armorId;
                    armor.currentArmor = armor.totalArmor;
                    foreach (var part in armor.parts)
                        part.SetActive(true);
                }
            }
        }

        public int GetArmorId(BodyPart bodyPart)
        {
            if (!armorIds.ContainsKey(bodyPart))
                return 0;
            return armorIds[bodyPart] + 1;
        }

        public int HitArmor(int damage, BodyPart bodyPart, BoltType boltType)
        {      
            var armorsCurr = armors.FindAll(x => x.bodyPart == bodyPart);
            if (armorsCurr == null || armorsCurr.Count == 0)
                return damage + (bodyPart == BodyPart.Head ? 1 : 0);

            if (!armorIds.ContainsKey(bodyPart))
                return damage + (bodyPart == BodyPart.Head ? 1 : 0);

            var armor = armorsCurr[armorIds[bodyPart]];
            if (armor.currentArmor <= 0)
                return damage + (bodyPart == BodyPart.Head ? 1 : 0);

            int diff = damage - armor.currentArmor;
            if (diff < 0)
                diff = 0;

            armor.currentArmor -= damage;
            if (armor.currentArmor < 0 || boltType == BoltType.Piercing)
            {
                armor.currentArmor = 0;
            }

            while (armor.parts.Count > armor.currentArmor)
            {
                int i = Random.Range(0, armor.parts.Count);
                armor.parts[i].transform.parent = null;
                armor.parts[i].AddComponent<PolygonCollider2D>();
                var rg = armor.parts[i].AddComponent<Rigidbody2D>();
                rg.AddForce(Vector2.left * (force + Random.Range(-0.5f * force, 0.5f * force)));
                rg.AddTorque(Random.Range(-250f, 250f));
                Destroy(armor.parts[i], destructTime);
                armor.parts.RemoveAt(i);
            }
            return diff;
        }
    }
}
