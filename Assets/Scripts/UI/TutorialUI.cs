﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Stickman
{
    public class TutorialUI : MonoBehaviour
    {
        [SerializeField]
        GameObject handHorizontalMove;
        float horizontalShift = 300f;

        [SerializeField]
        GameObject handVerticalMove;
        float verticalShift = 400f;

        [SerializeField]
        GameObject handArrow;

        [SerializeField]
        GameObject arrowSelection;

        int moveCount = 0;
        bool isArrowOn = false;

        void Start()
        {
            handVerticalMove.SetActive(false);
            handHorizontalMove.SetActive(false);
            arrowSelection.SetActive(false);
            Invoke("PushToArrow", 1f);
            MoveVertical(true);
        }

        void MoveVertical(bool toUp)
        {
            handVerticalMove.SetActive(true);
            handHorizontalMove.SetActive(false);
            handVerticalMove.transform.DOLocalMoveY(handVerticalMove.transform.localPosition.y +
                (toUp ? verticalShift : -verticalShift), 1.5f)
                .OnUpdate(() => SwipeController.OnSwipe(0f, toUp ? 1.5f : -1.5f))
                .OnComplete(() => 
                { 
                    moveCount++;
                    if (moveCount < 4)
                    {
                        MoveVertical(!toUp);
                    }
                    else
                    {
                        moveCount = 0;
                        MoveHorizontal(false);
                    }
                });
        }

        void MoveHorizontal(bool toLeft)
        {
            handHorizontalMove.SetActive(true);
            handVerticalMove.SetActive(false);
            handHorizontalMove.transform.DOLocalMoveY(handHorizontalMove.transform.localPosition.y +
                (toLeft ? -horizontalShift : horizontalShift), 1f)
                .OnUpdate(() => SwipeController.OnSwipe((toLeft ? 1.5f : -1.5f), 0f))
                .OnComplete(() =>
                {
                    moveCount++;
                    if (moveCount < 4)
                    {
                        MoveHorizontal(!toLeft);
                    }
                    else
                    {
                        moveCount = 0;
                        MoveVertical(true);
                    }
                });
        }
        
        void PushToArrow()
        {
            if (!gameObject.activeSelf)
                return;
            
            float time = 0.5f;
            var startPos = handArrow.transform.localPosition;
            handArrow.transform.DOLocalMove(startPos + Vector3.right * 50f + Vector3.down * 50f, time).OnComplete(() => handArrow.transform.DOLocalMove(startPos, time));
            handArrow.transform.DOScale(1f, time).OnComplete(() => {
                isArrowOn = !isArrowOn;
                arrowSelection.SetActive(isArrowOn); handArrow.transform.DOScale(1.5f, time);
                PlayerPrefs.SetInt("CurrentPlayerArrow", isArrowOn ? 1 : 0);
                Game.Instance.ChangeArrow();
                Invoke("PushToArrow", 3f);
            });
        }

        public void OnTutorClose()
        {
            PlayerPrefs.SetInt("CurrentPlayerArrow", 0);
            Game.Instance.ChangeArrow();
            handArrow.transform.DOKill();
            handHorizontalMove.transform.DOKill();
            handVerticalMove.transform.DOKill();
            gameObject.SetActive(false);
            Game.Instance.OnTutorComplete();
        }
    }
}
