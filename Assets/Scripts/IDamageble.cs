﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public interface IDamageble
    {
        void TakeDamage(int damage);
        void TakeDamage(int damage, BodyPart bodyPart, BoltType boltType);
    }
}
