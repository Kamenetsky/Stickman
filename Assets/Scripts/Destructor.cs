﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class Destructor : MonoBehaviour
    {
        [SerializeField]
        List<GameObject> bodyParts = new List<GameObject>();
        Dictionary<GameObject, Color> colorByPart = new Dictionary<GameObject, Color>();

        [SerializeField]
        GameObject weapon;

        [SerializeField]
        float dieForce = 2500f;

        [Header("Freeze")]
        [SerializeField]
        GameObject ice;
        [SerializeField]
        ParticleSystem iceBreake;
        [SerializeField]
        Color freezColor = Color.white;

        [Header("Fire")]
        [SerializeField]
        ParticleSystem fire;

        public void Awake()
        {
            if (ice)
                ice.SetActive(false);
            if (iceBreake)
                iceBreake.gameObject.SetActive(false);
            if (fire)
                fire.gameObject.SetActive(false);
            foreach(var it in bodyParts)
            {
                var hj = it.GetComponent<HingeJoint2D>();
                if (hj != null)
                    hj.enabled = false;

                var sprt = it.GetComponent<SpriteRenderer>();
                if (sprt != null)
                {
                    colorByPart.Add(it, sprt.color);
                }
            }
        }

        public void Burn()
        {
            if (fire == null)
                return;
            fire.gameObject.SetActive(true);
            fire.Play();
        }

        public void Freeze(bool val = true)
        {
            if (ice)
                ice.SetActive(val);
            foreach(var it in bodyParts)
            {
                var sprt = it.GetComponent<SpriteRenderer>();
                if (sprt != null)
                {
                    sprt.color = val ? freezColor : colorByPart[it];
                }
            }
        }

        public void IceBreake()
        {
            foreach (var it in bodyParts)
            {
                var ik = it.GetComponent<SimpleCCD>();
                if (ik != null)
                    Destroy(ik);

                it.transform.parent = null;

                var hj = it.GetComponent<HingeJoint2D>();
                if (hj != null)
                    Destroy(hj);

                var rig = it.GetComponent<Rigidbody2D>();
                if (rig == null)
                    rig = it.AddComponent<Rigidbody2D>();
                rig.isKinematic = false;
                rig.constraints = RigidbodyConstraints2D.FreezePositionX;

                Destroy(it, 2f);
            }

            if (iceBreake == null)
                return;
            iceBreake.gameObject.SetActive(true);
            iceBreake.Play();
        }

        void DestructWeapon(float destructTime)
        {
            if (weapon != null)
            {
                var bolt = weapon.GetComponentInChildren<Bolt>();
                if (bolt != null)
                {
                    bolt.transform.parent = null;
                    bolt.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    Destroy(bolt.gameObject, destructTime);
                }

                weapon.transform.parent = null;
                var rig = weapon.AddComponent<Rigidbody2D>();
                rig.bodyType = RigidbodyType2D.Dynamic;
                Destroy(weapon, destructTime);
            }
        }

        public void Destruct(float destructTime)
        {
            foreach(var it in bodyParts)
            {
                var ik = it.GetComponent<SimpleCCD>();
                if (ik != null)
                    Destroy(ik);

                var hj = it.GetComponent<HingeJoint2D>();
                if (hj != null)
                    hj.enabled = true;

                var col = it.GetComponent<Collider2D>();
                if (col == null)
                    col = it.AddComponent<CapsuleCollider2D>();
                col.isTrigger = false;

                var rig = it.GetComponent<Rigidbody2D>();
                if (rig == null)
                    rig = it.AddComponent<Rigidbody2D>();
                rig.bodyType = RigidbodyType2D.Dynamic;
                rig.gravityScale = 1f;
                rig.freezeRotation = false;

                Destroy(it, destructTime);
            }
            bodyParts[0].GetComponent<Rigidbody2D>().AddForce(Vector2.right * dieForce * Random.Range(0.8f, 1.5f));

            DestructWeapon(destructTime);
        }
    }
}
