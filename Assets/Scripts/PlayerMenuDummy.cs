﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Stickman
{

    public class PlayerMenuDummy : MonoBehaviour
    {
        [SerializeField]
        Transform pelvis;
        [SerializeField]
        float moveIdleShift = 0.1f;
        [SerializeField]
        float moveIdleTime = 0.8f;

        [SerializeField]
        LineRenderer bowSpring;
        [SerializeField]
        Transform upperPoint;
        [SerializeField]
        Transform downPoint;
        [SerializeField]
        Transform arm;

        [SerializeField]
        Transform targetLeft;
        [SerializeField]
        Transform targetRight;

        void Start()
        {
            MoveIdle();
            MoveTargetRight();
            MoveTargetLeft();
        }

        void MoveTargetRight()
        {
            targetRight.DOLocalMoveY(Random.Range(4f, 6f), Random.Range(0.7f, 1.5f)).OnComplete(() =>
            {
                MoveTargetRight();
            });
        }

        void MoveTargetLeft()
        {
            targetLeft.DOLocalMoveX(Random.Range(0.15f, 1f), Random.Range(0.8f, 2f)).OnComplete(() =>
            {
                MoveTargetLeft();
            });
        }

        void MoveIdle()
        {
            if (pelvis == null)
                return;

            pelvis.DOLocalMoveX(pelvis.localPosition.x + moveIdleShift, moveIdleTime).OnComplete(() =>
            {
                pelvis.DOLocalMoveX(pelvis.localPosition.x - moveIdleShift, moveIdleTime).OnComplete(() =>
                { MoveIdle(); });
            });
        }

        void Update()
        {
            if (bowSpring != null)
            {
                bowSpring.SetPosition(0, upperPoint.position);
                bowSpring.SetPosition(1, arm.position);
                bowSpring.SetPosition(2, downPoint.position);
            }
        }
    }
}
