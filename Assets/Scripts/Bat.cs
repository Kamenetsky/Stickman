﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class Bat : MonoBehaviour, IDamageble
    {
        [SerializeField]
        ParticleSystem dieSystem;

        [SerializeField]
        SpriteRenderer sprite;

        [SerializeField]
        GameObject spit;
        [SerializeField]
        float spitDelay = 3f;
        [SerializeField]
        float speed = 1f;

        [SerializeField]
        Transform spitOrigin;

        [SerializeField]
        int prize = 25;

        float spitTime = 0f;
        bool isDead = false;

        void Start()
        {
            dieSystem.gameObject.SetActive(false);
            spitTime = spitDelay;
        }

        void Update()
        {
            spitTime -= Time.deltaTime;

            if (spitTime < 0f && !isDead)
            {
                spitTime = spitDelay;
                var spitObj = Instantiate(spit);
                spitObj.transform.position = spitOrigin.position;
                GameSoundController.instance.PlaySound("BatSpit");
            }
            
            transform.Translate(Vector3.left * speed * Time.deltaTime);

            if (transform.position.x < -60f)
                Destroy(gameObject);
        }

        public void TakeDamage(int damage)
        {
            Die();
        }

        public void TakeDamage(int damage, BodyPart bodyPart, BoltType boltType)
        {
            Die();
        }

        void Die()
        {
            if (isDead)
                return;

            isDead = true;

            Game.Instance.SpawnBonus(transform.position + Vector3.up * 3f, prize);
            Game.OnBotDie(Game.Instance.GetScores(), prize);

            var bolts = GetComponentsInChildren<Bolt>();
            foreach (var it in bolts)
            {
                Destroy(it.gameObject);
            }
            sprite.enabled = false;
            dieSystem.gameObject.SetActive(true);
            dieSystem.Play();
            Destroy(gameObject, 1f);
        }
    }
}
