﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Stickman
{
    public class Background : MonoBehaviour
    {
        public static System.Action<DayPart> OnChangeDayPart = delegate { };
        
        public enum DayPart
        {
            Day = 0,
            Evening,
            Night
        }

        [System.Serializable]
        public class BackgroundInfo
        {
            public DayPart dayPart = DayPart.Day;
            public float duration = 240;
            public GameObject sky;
            public int cloudCount = 4;
            public float cloudTime = 30f;
            public Color cloudColor = Color.white;
            public int starsCount = 0;
        }

        [SerializeField]
        List<BackgroundInfo> backgrounds = new List<BackgroundInfo>();

        BackgroundInfo currentBackground;
        List<GameObject> clouds = new List<GameObject>();
        List<GameObject> stars = new List<GameObject>();

        [SerializeField]
        GameObject[] cloudPrefabs;

        [SerializeField]
        GameObject[] starPrefabs;

        [SerializeField]
        GameObject moon;

        [SerializeField]
        Vector2 cloudShiftX = new Vector2(-45f, 45f);

        [SerializeField]
        Vector2 cloudShiftY = new Vector2(6f, 20f);

        [SerializeField]
        Vector2 starsShiftX = new Vector2(-35f, 35f);

        [SerializeField]
        Vector2 starsShiftY = new Vector2(-5f, 23f);

        [SerializeField]
        float changeTime = 5f;
        
        float currTime = 0f;
        bool changeDayPart = false;

        void Awake()
        {
            foreach (var it in backgrounds)
            {
                it.sky.SetActive(false);
            }

            foreach (var it in cloudPrefabs)
            {
                it.SetActive(false);
            }

            foreach (var it in starPrefabs)
            {
                it.SetActive(false);
            }
            changeDayPart = true;
        }

        public DayPart GetDayPart()
        {
            return currentBackground.dayPart;
        }

        public void SetDayPart(DayPart dayPart)
        {
            moon.SetActive(dayPart == DayPart.Night);
            currentBackground = backgrounds.Find(x => x.dayPart == dayPart);
            currentBackground.sky.SetActive(true);

            for (int i = 0; i < currentBackground.cloudCount; ++i)
                SpawnCloud(true);

            for (int i = 0; i < currentBackground.starsCount; ++i)
                SpawnStar();
        }

        void SpawnCloud(bool begin = false)
        {
            var cloud = Instantiate(cloudPrefabs[Random.Range(0, cloudPrefabs.Length)]);
            cloud.transform.parent = gameObject.transform;
            cloud.transform.position = new Vector3(begin ? Random.Range(starsShiftX.x, starsShiftX.y) : cloudShiftX.x, Random.Range(cloudShiftY.x, cloudShiftY.y), 0f);
            cloud.transform.localScale = cloud.transform.localScale * Random.Range(0.8f, 1.2f);
            var sr = cloud.GetComponent<SpriteRenderer>();
            sr.flipX = Random.value > 0.5f;
            sr.color = currentBackground.cloudColor;
            clouds.Add(cloud);
            cloud.SetActive(true);
            cloud.transform.DOMoveX(cloudShiftX.y, (cloudShiftX.y - cloud.transform.position.x) / (cloudShiftX.y - cloudShiftX.x) * currentBackground.cloudTime * Random.Range(0.8f, 1.2f))
                .SetEase(Ease.Linear)
                .OnComplete(() =>
            { 
                clouds.Remove(cloud); Destroy(cloud);
                if (clouds.Count < currentBackground.cloudCount) 
                    SpawnCloud();
            });
        }
        
        void SpawnStar()
        {
            var star = Instantiate(starPrefabs[Random.Range(0, starPrefabs.Length)]);
            star.transform.parent = gameObject.transform;
            star.transform.position = new Vector3(Random.Range(starsShiftX.x, starsShiftX.y), Random.Range(starsShiftY.x, starsShiftY.y), 0f);
            star.transform.localScale = star.transform.localScale * Random.Range(1f, 3f);
            stars.Add(star);
            star.SetActive(true);
            var sr = star.GetComponent<SpriteRenderer>();
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 0f);
            FadeStar(sr, true);
        }

        void FadeStar(SpriteRenderer star, bool forward)
        {
            float val = forward ? Random.Range(0.3f, 0.7f) : 1f;
            float time = Random.Range(1.5f, 3f);
            forward = ! forward;
            star.DOFade(val, time).OnComplete(() => FadeStar(star, forward));
        }

        void Update()
        {
            if (!changeDayPart)
                return;

            currTime += Time.deltaTime;

            if (currTime > currentBackground.duration)
            {
                currTime = 0f;
                NextDayPart();
            }
        }

        void NextDayPart()
        {
            DayPart next;
            if (currentBackground.dayPart == DayPart.Day)
                next = DayPart.Evening;
            else if (currentBackground.dayPart == DayPart.Evening)
                next = DayPart.Night;
            else
                next = DayPart.Day;

            OnChangeDayPart(next);

            var prevBackground = currentBackground;
            currentBackground = backgrounds.Find(x => x.dayPart == next);

            moon.SetActive(true);
            var moonSr = moon.GetComponent<SpriteRenderer>();
            moonSr.color = new Color(moonSr.color.r, moonSr.color.g, moonSr.color.b, 0f);
            moonSr.DOFade(next == DayPart.Night ? 1f : 0f, changeTime);
            currentBackground.sky.SetActive(true);
            var sr = currentBackground.sky.GetComponent<SpriteRenderer>();
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 0f);
            sr.DOFade(1f, changeTime);

            prevBackground.sky.GetComponent<SpriteRenderer>().DOFade(0f, changeTime).OnComplete(() => prevBackground.sky.SetActive(false));
            foreach (var it in clouds)
            {
                it.GetComponent<SpriteRenderer>().DOColor(currentBackground.cloudColor, changeTime);
            }

            if (stars.Count < currentBackground.starsCount)
            {
                for(int i = stars.Count; i < currentBackground.starsCount; ++i)
                    SpawnStar();
            }
            else
            {
                while (stars.Count > currentBackground.starsCount)
                {
                    var star = stars[Random.Range(0, stars.Count)];
                    stars.Remove(star);
                    var starSr = star.GetComponent<SpriteRenderer>();
                    starSr.DOComplete();
                    starSr.DOFade(0f, changeTime).OnComplete(() => { Destroy(star); });
                }
            }
        }
    }
}
