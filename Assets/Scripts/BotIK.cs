﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class BotIK : PlayerBase
    {
        protected PlayerIK player = null;

        [SerializeField]
        protected float minAimingTime = 1.5f;
        [SerializeField]
        protected float maxAimingTime = 3f;
        protected float currAimingTime;

        protected float aimOffsetMax = 5f;
        protected float aimOffsetMin = -5f;
        protected float currentAimOffset;

        public override void Start()
        {
            damageAngle = -45f;
            FindPlayer();
            SetAimingTime();
            base.Start();
            SetAim();
            life = GameVariables.botLifeBase;
            initLife = life;
            freezTime = GameVariables.botFreezTime;
            aimOffsetMin = GameVariables.botWeaponAimingIntervals[weapon].x;
            aimOffsetMax = GameVariables.botWeaponAimingIntervals[weapon].y;
        }

        public void Generate(BotEquip equip)
        {
            currentBoltType = equip.bolt;
            armorController.SetArmors(new Dictionary<BodyPart, int>() { { BodyPart.Head, (int)equip.headArmor - 1}, { BodyPart.BodyPart, (int)equip.bodyArmor - 1} });
            SetShield(equip.shield);
        }

        protected void FindPlayer()
        {
            player = FindObjectOfType<PlayerIK>();
        }

        protected virtual void SetAim()
        {
            if (player != null)
            {
                currentAimOffset = player.GetHead().position.y + Random.Range(aimOffsetMin, aimOffsetMax);
            }

            currentForce = Random.Range(minForce, maxForce);
        }

        protected virtual void SetAimingTime()
        {
            currAimingTime = Random.Range(minAimingTime, maxAimingTime);
            currAimingTime = Mathf.Max(cooldown, currAimingTime);
        }

        protected override void Update()
        {
            if (player == null || isDead)
                return;

            base.Update();

            if (isFrozen)
                return;

            currTime += Time.deltaTime;
            if (currTime >= currAimingTime)
            {
                currTime = 0;
                Fire();
                SetAimingTime();
                SetAim();
            }
        }
    }
}
