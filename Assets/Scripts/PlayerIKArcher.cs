﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public class PlayerIKArcher : PlayerIK
    {
        [SerializeField]
        Transform arm;
        [SerializeField]
        float maxXShift = 1.3f;
        [SerializeField]
        float armSpeed = 0.05f;
        float armStartX;

        [SerializeField]
        LineRenderer bowSpring;
        [SerializeField]
        Transform upperPoint;
        [SerializeField]
        Transform downPoint;

        public override void Start()
        {
            base.Start();
            armStartX = arm.localPosition.x;
        }

        protected override void OnSwipe(float x, float y)
        {
            base.OnSwipe(x, y);
            if (!isActive || !isAiming || isFrozen)
                return;

            float diff = Mathf.Clamp(arm.localPosition.x + x * armSpeed, armStartX - maxXShift, armStartX);
            arm.localPosition = new Vector3(diff, arm.localPosition.y, arm.localPosition.z);
            currentForce = Mathf.Lerp(minForce, maxForce, (new Vector3(armStartX, arm.localPosition.y, arm.localPosition.z) - arm.localPosition).magnitude / maxXShift);
        }

        protected override void Update()
        {
            base.Update();

            if (bowSpring != null)
            {
                bowSpring.SetPosition(0, upperPoint.position);
                bowSpring.SetPosition(1, arm.position);
                bowSpring.SetPosition(2, downPoint.position);
            }
        }

        protected override void Fire()
        {
            base.Fire();
            
            arm.localPosition = new Vector3(armStartX, arm.localPosition.y, arm.localPosition.z);
        }
    }
}
