﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] Transform hand;
    [SerializeField] LineRenderer handLine;
    [SerializeField] Transform handLineAnchor1;
    [SerializeField] Transform handLineAnchor2;
    [SerializeField] float angleLimitMax = 60f;
    [SerializeField] float angleLimitMin = -100f;
    [SerializeField] float cooldown = 1f;

    [SerializeField] GameObject boltPrefab;
    [SerializeField] Transform boltPlaceHolder;
    GameObject currentBolt;

    [SerializeField] float maxForce = 800f;
    [SerializeField] float minForce = 100f;
    [SerializeField] float timeSpeed = 0.5f;
    float currentForce;
    float currTime;

    [SerializeField] Image forceProgress;

    bool isActive = true;

    void Awake()
    {
        SwipeController.OnSwipe += OnSwipe;
    }

    private void Start()
    {
        currentForce = minForce;
        CreateBolt();
    }

    void OnDestroy()
    {
        SwipeController.OnSwipe -= OnSwipe;
    }

    private void Update()
    {
        currTime += timeSpeed * Time.deltaTime;
        forceProgress.fillAmount = currTime;
        if (currTime < 0f || currTime > 1f)
            timeSpeed = -timeSpeed;
        currentForce = Mathf.Lerp(minForce, maxForce, currTime);
        handLine.SetPosition(0, handLineAnchor1.position);
        handLine.SetPosition(1, handLineAnchor2.position);
    }

    void CreateBolt()
    {
        currentBolt = Instantiate(boltPrefab);
        currentBolt.transform.SetParent(boltPlaceHolder, false);
        currentBolt.transform.localPosition = Vector3.zero;
        currentBolt.transform.localRotation = Quaternion.identity;
    }

    public void Fire()
    {
        if (currentBolt == null)
            return;

        currentBolt.transform.parent = null;
        var bolt = currentBolt.GetComponent<Bolt>();
        bolt.force = currentForce;
        bolt.Fire();
        currentBolt = null;
        Invoke("CreateBolt", cooldown);
    }

    void OnSwipe(float x, float y)
    {
        if (!isActive)
            return;

        hand.eulerAngles = new Vector3(0f, 0f, ClampAngle(hand.eulerAngles.z + y * 0.3f, angleLimitMin, angleLimitMax));
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = Mathf.Repeat(angle, 360);
        min = Mathf.Repeat(min, 360);
        max = Mathf.Repeat(max, 360);
        bool inverse = false;
        var tmin = min;
        var tangle = angle;
        if (min > 180)
        {
            inverse = !inverse;
            tmin -= 180;
        }
        if (angle > 180)
        {
            inverse = !inverse;
            tangle -= 180;
        }
        var result = !inverse ? tangle > tmin : tangle < tmin;
        if (!result)
            angle = min;

        inverse = false;
        tangle = angle;
        var tmax = max;
        if (angle > 180)
        {
            inverse = !inverse;
            tangle -= 180;
        }
        if (max > 180)
        {
            inverse = !inverse;
            tmax -= 180;
        }

        result = !inverse ? tangle < tmax : tangle > tmax;
        if (!result)
            angle = max;
        return angle;
    }
}*/
