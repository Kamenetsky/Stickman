﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Stickman
{
    public class PlayerBase : MonoBehaviour, IDamageble
    {
        public static System.Action<PlayerBase> OnDie = delegate { };
        public static System.Action<BoltType> OnFire = delegate { };
        
        public static Dictionary<string, BodyPart> bodyTags = new Dictionary<string, BodyPart>()
        {
            {"Head", BodyPart.Head},
            {"BodyPart", BodyPart.BodyPart}
        };

        [SerializeField]
        protected WeaponType weapon;
        protected ShieldType shield = ShieldType.None;

        [SerializeField]
        protected Transform aim;
        [SerializeField]
        protected float topAimOffset = 10f;
        [SerializeField]
        protected float bottompAimOffset = -10f;
        [SerializeField]
        protected float aimSpeed = 0.5f;
        protected float aimStartX;

        [SerializeField]
        protected float cooldown = 1f;

        [SerializeField]
        GameObject boltPrefab;
        [SerializeField]
        Transform boltPlaceHolder;
        protected GameObject currentBolt;
        protected BoltType currentBoltType = BoltType.Iron;

        [SerializeField]
        protected float maxForce = 800f;
        [SerializeField]
        protected float minForce = 100f;
        protected float currentForce;
        protected float currTime;

        protected bool isActive = true;
        protected bool isAiming = false;

        [SerializeField]
        protected Transform head;
        protected float damageAngle = 45f;
        protected bool isDamaged = false;
        protected bool isDead = false;
        protected bool isFrozen = false;

        [SerializeField]
        protected Transform pelvis;
        [SerializeField]
        protected bool moveIdle = false;
        [SerializeField]
        protected float moveIdleShift = 0.1f;
        [SerializeField]
        protected float moveIdleTime = 0.8f;

        [SerializeField]
        protected float destructTime = 5f;

        protected float freezTime = 2f;

        protected Destructor destructor;
        protected ArmorController armorController;
        protected int life = 2;
        protected int initLife;

        [Header("UI")]
        [SerializeField]
        UnityEngine.UI.Slider lifeProgress;

        [Header("Shields")]
        [SerializeField]
        GameObject[] shields;

        protected bool isBot = false;

        public virtual void Awake()
        {
            destructor = GetComponent<Destructor>();
            armorController = GetComponent<ArmorController>();
            isBot = this is BotIK;
            foreach (var it in shields)
            {
                it.SetActive(false);
            }
        }
        
        public virtual void Start()
        {
            life = GameVariables.playerLifeBase;
            initLife = life;
            freezTime = GameVariables.playerFreezTime;
            SetHealth(true);
            currentForce = minForce;
            aimStartX = aim.localPosition.x;
            CreateBolt();
            MoveIdle();
        }

        public void Heal()
        {
            if (isDead)
                return;
            life = initLife;
            SetHealth();
        }

        public bool IsFullHealth()
        {
            return life == initLife;
        }

        public bool IsDead()
        {
            return isDead;
        }

        public void SetShield(ShieldType shield)
        {
            this.shield = shield;
            int id = (int)shield;
            if (id < 1)
                return;
            shields[id - 1].SetActive(true);
        }

        public bool IsBot()
        {
            return isBot;
        }

        protected void MoveIdle(bool loop = true)
        {
            if (pelvis == null || !moveIdle)
                return;

            pelvis.DOLocalMoveX(pelvis.localPosition.x + moveIdleShift, moveIdleTime).OnComplete(() =>
            {
                pelvis.DOLocalMoveX(pelvis.localPosition.x - moveIdleShift, moveIdleTime).OnComplete(() =>
                { if (loop) MoveIdle(loop); });
            });
        }

        public bool HasBolt()
        {
            return currentBolt != null;
        }

        public virtual void CreateBolt()
        {
            if (currentBolt != null)
                Destroy(currentBolt);
            isAiming = true;
            currentBolt = Instantiate(boltPrefab);
            if (!isBot)
                currentBoltType = (BoltType)PlayerPrefs.GetInt("CurrentPlayerArrow", 0);
            currentBolt.GetComponent<Bolt>().Init(currentBoltType, GameVariables.weaponDamage[weapon]);
            currentBolt.transform.SetParent(boltPlaceHolder, false);
            currentBolt.transform.localPosition = Vector3.zero;
            currentBolt.transform.localRotation = Quaternion.identity;
        }

        protected virtual void Fire()
        {
            if (currentBolt == null)
                return;
            isAiming = false;
            currentBolt.transform.parent = null;
            var bolt = currentBolt.GetComponent<Bolt>();
            bolt.force = currentForce;
            bolt.Fire();
            currentBolt = null;
            if (!isBot)
                OnFire(currentBoltType);
            Invoke("CreateBolt", cooldown);

            GameSoundController.instance.PlaySound(currentBoltType == BoltType.Fire ? "FireArrowThrow" : (weapon == WeaponType.Spear ? "SpearThrow" : "SimpleArrowTrow"));
        }

        void DamageEffect()
        {
            GameSoundController.instance.PlaySound("ArrowHitBody");
            GameSoundController.instance.PlaySound("ManDamage");
            if (head != null && !isDamaged)
            {
                isDamaged = true;
                head.DOLocalRotate(head.localEulerAngles - Vector3.back * damageAngle, 0.15f).OnComplete(() =>
                {
                    head.DOLocalRotate(head.localEulerAngles + Vector3.back * damageAngle, 0.15f).OnComplete(() =>
                    { isDamaged = false; });
                });
            }
        }

        void SetHealth(bool instant = false)
        {
            if (lifeProgress == null)
                return;

            lifeProgress.DOValue(((float)life) / ((float)initLife), 0.3f);
            if (instant)
                lifeProgress.value = ((float)life) / ((float)initLife);
        }
        
        public virtual void TakeDamage(int damage)
        {
            if (isDead)
                return;

            DamageEffect();

            life -= damage;
            SetHealth();
            if (life <= 0)
            {
                Die();
            }
        }
        
        public virtual void TakeDamage(int damage, BodyPart bodyPart, BoltType boltType)
        {
            if (isDead)
                return;

            DamageEffect();
            
            if (armorController)
                life -= armorController.HitArmor(damage, bodyPart, boltType);
            else
                life -= damage;

            if (boltType == BoltType.Frost)
            {
                Freeze(true);
            }
            else if (boltType == BoltType.Fire)
            {
                StartCoroutine(Burn());
            }
            else if (boltType == BoltType.Piercing && isFrozen)
            {
                destructor.IceBreake();
                life = 0;
            }

            SetHealth();

            if (life <= 0)
            {
                Die();
            }
        }

        public virtual void Die()
        {
            if (isDead)
                return;
            isDead = true;
            lifeProgress.gameObject.SetActive(false);
            destructor.Destruct(destructTime);
            foreach (var it in shields)
            {
                if (it && it.activeSelf)
                    it.GetComponent<Shield>().Destruct();
            }
            var renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach (var it in renderers)
            {
                it.DOFade(0f, destructTime - 0.5f);
            }
            Destroy(gameObject, destructTime);
            OnDie(this);
        }

        public virtual void Freeze(bool val)
        {
            isFrozen = val;
            currTime = 0f;
            destructor.Freeze(val);
            if (val)
            {
                head.DOPause();
                pelvis.DOPause();
            }
            else
            {
                head.DOPlay();
                pelvis.DOPlay();
            }
        }

        public virtual IEnumerator Burn()
        {
            if (isDead)
                yield break;
            yield return new WaitForSeconds(0.7f * GameVariables.burnInterval);
            if (!isDead)
                destructor.Burn();
            yield return new WaitForSeconds(0.2f * GameVariables.burnInterval);
            TakeDamage(1);
        }

        protected virtual void Update()
        {
            if (isFrozen)
            {
                currTime += Time.deltaTime;
                if (currTime > freezTime)
                {
                    Freeze(false);
                }
                return;
            }
        }

        public Transform GetHead()
        {
            return head;
        }

        public int GetLevel()
        {
            return armorController.GetArmorId(BodyPart.Head) + armorController.GetArmorId(BodyPart.BodyPart) + (int)weapon + (int)shield;
        }
    }
}
