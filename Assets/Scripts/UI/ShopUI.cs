﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Stickman
{
    public class ShopUI : MonoBehaviour
    {      
        [SerializeField]
        Text crystalsText;

        ShopItem[] items;

        void Awake()
        {
            items = GetComponentsInChildren<ShopItem>(true);
        }

        void OnEnable()
        {
            UpdateCrystals();
            Refresh();
        }

        void UpdateCrystals()
        {
            crystalsText.text = PlayerPrefs.GetInt("crystals", 0).ToString();
        }

        public void OnBuyArrowItem(ShopItemArrow item)
        {
            if (item.price > PlayerPrefs.GetInt("crystals", 0))
                return;

            PlayerPrefs.SetInt("crystals", PlayerPrefs.GetInt("crystals", 0) - item.price);
            PlayerPrefs.SetInt("BoltAmount_" + item.boltType.ToString(), PlayerPrefs.GetInt("BoltAmount_" + item.boltType.ToString(), 0) + item.packAmount);
            item.Refresh();
            Refresh();
        }

        public void OnBuyWeaponItem(ShopItemWeapon item)
        {
            if (item.price > PlayerPrefs.GetInt("crystals", 0))
                return;

            PlayerPrefs.SetInt("crystals", PlayerPrefs.GetInt("crystals", 0) - item.price);
            PlayerPrefs.SetInt("isWeaponBouth_" + item.weaponType.ToString(), 1);
            item.Refresh();
            Refresh();

            int currWeapon = PlayerPrefs.GetInt("playerWeaponIndex", 0);

            if (currWeapon == 0)
                OnEquipWeaponItem(item);
        }

        public void OnEquipWeaponItem(ShopItemWeapon item)
        {
            PlayerPrefs.SetInt("playerWeaponIndex", (int)item.weaponType);
            item.Refresh();
            Refresh();
        }

        public void OnBuyArmorItem(ShopItemArmor item)
        {
            if (item.price > PlayerPrefs.GetInt("crystals", 0))
                return;

            int currArmor = PlayerPrefs.GetInt("player" + item.prefix + "ArmodIndex", -1);
            PlayerPrefs.SetInt("crystals", PlayerPrefs.GetInt("crystals", 0) - item.price);
            PlayerPrefs.SetInt("isArmorBouth" + item.prefix + item.armorType.ToString(), 1);
            item.Refresh();
            Refresh();

            if (currArmor < (int)item.armorType - 1)
                OnEquipArmorItem(item);
        }

        public void OnEquipArmorItem(ShopItemArmor item)
        {
            PlayerPrefs.SetInt("player" + item.prefix + "ArmodIndex", (int)item.armorType - 1);
            item.Refresh();
            Refresh();
        }

        public void OnBuyShieldItem(ShopItemShield item)
        {
            if (item.price > PlayerPrefs.GetInt("crystals", 0))
                return;

            int currShield = PlayerPrefs.GetInt("playerShieldIndex", 0);
            PlayerPrefs.SetInt("crystals", PlayerPrefs.GetInt("crystals", 0) - item.price);
            PlayerPrefs.SetInt("isShieldBouth" + item.shieldType.ToString(), 1);
            item.Refresh();
            Refresh();

            if (currShield < (int)item.shieldType)
                OnEquipShieldItem(item);
        }

        public void OnEquipShieldItem(ShopItemShield item)
        {
            PlayerPrefs.SetInt("playerShieldIndex", (int)item.shieldType);
            item.Refresh();
            Refresh();
        }

        public void Refresh()
        {
            foreach (var it in items)
            {
                it.Refresh();
            }
            UpdateCrystals();
        }

        public void OnCheat()
        {
            PlayerPrefs.SetInt("crystals", PlayerPrefs.GetInt("crystals", 0) + 1000);
            UpdateCrystals();
        }
    }
}
