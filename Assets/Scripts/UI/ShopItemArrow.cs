﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Stickman
{
    public class ShopItemArrow : ShopItem
    {
        public int packAmount;
        public BoltType boltType;

        [SerializeField]
        Text currentAmountText;
        [SerializeField]
        Text packAmountText;

        protected override void Start()
        {
            base.Start();
            packAmountText.text = "x" + packAmount.ToString();
        }

        public override void Refresh()
        {
            base.Refresh();
            currentAmountText.text = PlayerPrefs.GetInt("BoltAmount_" + boltType.ToString(), 0).ToString();
        }
    }
}
