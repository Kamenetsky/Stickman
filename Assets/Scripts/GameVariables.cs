﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stickman
{
    public enum ArmorType
    {
        None = 0,
        Leather,
        Iron,
        Gold
    }
    
    public enum BodyPart
    {
        Head,
        BodyPart
    }

    public enum WeaponType
    {
        Bow = 0,
        BowRelic,
        Crossbow,
        Spear
    }

    public enum BoltType
    {
        Iron = 0,
        Fire,
        Frost,
        Piercing
    }

    public enum ShieldType
    {
        None = 0,
        Wood,
        Stone,
        Iron
    }

    public class BotEquip
    {
        public WeaponType weapon;
        public ArmorType headArmor;
        public ArmorType bodyArmor;
        public BoltType bolt;
        public ShieldType shield;

        public BotEquip(WeaponType weapon, ArmorType headArmor, ArmorType bodyArmor, BoltType bolt, ShieldType shield)
        {
            this.weapon = weapon;
            this.headArmor = headArmor;
            this.bodyArmor = bodyArmor;
            this.bolt = bolt;
            this.shield = shield;
        }
    }
    
    public static class GameVariables
    {
        public static int dragonMaxLife = 20;
        
        public static int playerLifeBase = 2;
        public static int botLifeBase = 2;

        public static float burnInterval = 3f;
        public static float playerFreezTime = 2f;
        public static float botFreezTime = 2.5f;

        public static Dictionary<ShieldType, int> shieldArmors = new Dictionary<ShieldType, int>()
        {
            {ShieldType.Wood, 3},
            {ShieldType.Stone, 5},
            {ShieldType.Iron, 7}
        };

        public static Dictionary<WeaponType, Vector2> botWeaponAimingIntervals = new Dictionary<WeaponType, Vector2>()
        {
            {WeaponType.Bow, new Vector2(-10, 40)},
            {WeaponType.BowRelic, new Vector2(-7, 35)},
            {WeaponType.Crossbow, new Vector2(-12, 12)},
            {WeaponType.Spear, new Vector2(-10, 10)}
        };

        public static float aimingPercentBylevel = 5f;

        public static Dictionary<WeaponType, int> weaponDamage = new Dictionary<WeaponType, int>()
        {
            {WeaponType.Bow, 1},
            {WeaponType.BowRelic, 1},
            {WeaponType.Crossbow, 2},
            {WeaponType.Spear, 2}
        };

        public static List<int> armorValues = new List<int> { 1, 2, 3, 5, 7 };

        public static List<List<BotEquip>> botEquipByLevel = new List<List<BotEquip>> 
        {
            new List<BotEquip> {new BotEquip(WeaponType.Bow, ArmorType.None, ArmorType.None, BoltType.Iron, ShieldType.None)},
            new List<BotEquip> {new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.None, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.None, ArmorType.Leather, BoltType.Iron, ShieldType.None)},
            new List<BotEquip> {new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.Leather, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.None, ArmorType.Leather, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.None, BoltType.Fire, ShieldType.None)},
            new List<BotEquip> {new BotEquip(WeaponType.BowRelic, ArmorType.Leather, ArmorType.Leather, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.Leather, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.None, ArmorType.None, BoltType.Iron, ShieldType.Wood)},
            new List<BotEquip> {new BotEquip(WeaponType.Bow, ArmorType.Iron, ArmorType.Leather, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.Iron, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Iron, ArmorType.None, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.None, ArmorType.Iron, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.None, BoltType.Iron, ShieldType.Wood),
                                new BotEquip(WeaponType.Bow, ArmorType.None, ArmorType.Leather, BoltType.Iron, ShieldType.Wood)},
            new List<BotEquip> {new BotEquip(WeaponType.Bow, ArmorType.Iron, ArmorType.Iron, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.None, ArmorType.None, BoltType.Iron, ShieldType.Stone)},
            new List<BotEquip> {new BotEquip(WeaponType.BowRelic, ArmorType.Leather, ArmorType.Iron, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.Iron, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Iron, ArmorType.Leather, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.Leather, BoltType.Frost, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.Leather, BoltType.Iron, ShieldType.Wood)},
            new List<BotEquip> {new BotEquip(WeaponType.Bow, ArmorType.Iron, ArmorType.Gold, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Gold, ArmorType.Iron, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Iron, ArmorType.Leather, BoltType.Frost, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.Leather, BoltType.Frost, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Iron, ArmorType.Iron, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Leather, ArmorType.Leather, BoltType.Iron, ShieldType.Stone),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Iron, ArmorType.Iron, BoltType.Iron, ShieldType.None)},
            new List<BotEquip> {new BotEquip(WeaponType.BowRelic, ArmorType.Iron, ArmorType.Gold, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Iron, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Iron, ArmorType.Gold, BoltType.Frost, ShieldType.None),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Iron, BoltType.Frost, ShieldType.None),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Gold, ArmorType.Gold, BoltType.Fire, ShieldType.None)},
            new List<BotEquip> {new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Fire, ShieldType.None),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Iron, ShieldType.None),
                                new BotEquip(WeaponType.Bow, ArmorType.Gold, ArmorType.Gold, BoltType.Iron, ShieldType.Wood)},
            new List<BotEquip> {new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Fire, ShieldType.Wood),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Frost, ShieldType.Wood),
                                new BotEquip(WeaponType.Bow, ArmorType.Gold, ArmorType.Gold, BoltType.Piercing, ShieldType.None)},
            new List<BotEquip> {new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Fire, ShieldType.Stone),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Piercing, ShieldType.Stone),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Frost, ShieldType.Stone),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Frost, ShieldType.Wood),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Piercing, ShieldType.Wood),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Fire, ShieldType.Wood),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Iron, ShieldType.Iron)},
            new List<BotEquip> {new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Fire, ShieldType.Iron),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Piercing, ShieldType.Iron),
                                new BotEquip(WeaponType.BowRelic, ArmorType.Gold, ArmorType.Gold, BoltType.Frost, ShieldType.Iron),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Frost, ShieldType.Stone),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Piercing, ShieldType.Stone),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Fire, ShieldType.Stone),},
            new List<BotEquip> {new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Fire, ShieldType.Iron),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Piercing, ShieldType.Iron),
                                new BotEquip(WeaponType.Crossbow, ArmorType.Gold, ArmorType.Gold, BoltType.Frost, ShieldType.Iron)}
                                
        };
    }
}
