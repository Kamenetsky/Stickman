﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

namespace Stickman
{
    public class Shield : MonoBehaviour, IDamageble
    {
        [SerializeField]
        ShieldType shieldType;

        [SerializeField]
        ParticleSystem fireEffect;
        float burnTime = 2f;

        [SerializeField]
        ParticleSystem destructEffect;
        SpriteRenderer sprite;
        float destructTime = 0.5f;

        int life = 1;

        bool isDestructed = false;

        void Start()
        {
            life = GameVariables.shieldArmors[shieldType];
            if (destructEffect)
                destructEffect.gameObject.SetActive(false);
            if (fireEffect)
                fireEffect.gameObject.SetActive(false);
            sprite = GetComponent<SpriteRenderer>();
        }

        public virtual void TakeDamage(int damage)
        {
            if (isDestructed)
                return;

            life -= damage;

            if (life <= 0)
            {
                Destruct();
            }
        }

        public virtual void TakeDamage(int damage, BodyPart bodyPart, BoltType boltType)
        {
            if (isDestructed)
                return;

            GameSoundController.instance.PlaySound(shieldType == ShieldType.Wood ? "ArrowHitWood" : (shieldType == ShieldType.Stone ? "ArrowHitWood" : "ArrowHitMetal"));

            if (shieldType == ShieldType.Wood && boltType == BoltType.Fire)
            {
                Burn();
                return;
            }
            
            life -= damage;

            if (shieldType == ShieldType.Stone && boltType == BoltType.Piercing)
                life = 0;

            if (shieldType == ShieldType.Iron && boltType == BoltType.Piercing)
                life -= 2;

            if (life <= 0)
            {
                Destruct();
            }
        }

        void Burn()
        {
            isDestructed = true;
            PlayerPrefs.SetInt("playerShieldIndex", 0);
            PlayerPrefs.SetInt("isShieldBouth" + shieldType.ToString(), 0);
            RemoveArrows();
            if (fireEffect)
            {
                fireEffect.gameObject.SetActive(true);
                fireEffect.Play();
                fireEffect.transform.DOLocalMoveY(0f, burnTime);
            }
            sprite.transform.DOScaleY(0f, burnTime);
            sprite.DOColor(Color.black, burnTime);
            Destroy(gameObject, burnTime);
        }

        public void Destruct()
        {
            isDestructed = true;
            PlayerPrefs.SetInt("playerShieldIndex", 0);
            PlayerPrefs.SetInt("isShieldBouth" + shieldType.ToString(), 0);
            RemoveArrows();
            if (destructEffect)
            {
                destructEffect.gameObject.SetActive(true);
                destructEffect.Play();
            }
            sprite.DOFade(0f, destructTime);
            Destroy(gameObject, destructTime);
        }

        void RemoveArrows()
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                var child = transform.GetChild(i);
                if (child.GetComponentInChildren<Bolt>() == null)
                    continue;
                child.parent = null;
                var rb = child.gameObject.GetComponent<Rigidbody2D>();
                if (rb == null)
                    rb = child.gameObject.AddComponent<Rigidbody2D>();
            }
        }
    }
}
