﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

namespace Stickman
{
    public class GameUI : MonoBehaviour
    {
        [System.Serializable]
        public class ArrowButtonInfo
        {
            public BoltType boltType;
            public Toggle button;
            public Text amount;
        }
        
        [SerializeField]
        GameObject gamePanel;
        [SerializeField]
        GameObject menuPanel;
        [SerializeField]
        GameObject menuDecor;
        [SerializeField]
        GameObject shopPanel;
        [SerializeField]
        GameObject pausePanel;
        [SerializeField]
        GameObject losePanel;
        [SerializeField]
        GameObject tutorPanel;
        [SerializeField]
        GameObject privacyPanel;
        [SerializeField]
        GameObject titlePanel;
        [SerializeField]
        Text scoresText;
        [SerializeField]
        Text crystalsText;
        [SerializeField]
        Text loseScoresText;
        [SerializeField]
        Text loseMaxScoresText;
        int currentScores = 0;

        [SerializeField]
        ArrowButtonInfo[] buttonInfos;

        [SerializeField]
        Button healButton;
        [SerializeField]
        Image healProgress;
        int healCost = 15;
        float healCooldown = 10f;
        float healTime = 10f;

        [SerializeField]
        Text bonusLevelText;
        
        void Awake()
        {
            Game.OnLose += OnLose;
            Game.OnBotDie += OnBotDie;
            PlayerBase.OnFire += OnPlayerFire;
            bonusLevelText.gameObject.SetActive(false);

            if (PlayerPrefs.GetInt("IsFirstTimeRun", 1) == 1)
            {
                OnPrivacy(true);
                PlayerPrefs.SetInt("IsFirstTimeRun", 0);
            }
        }

        void OnDestroy()
        {
            Game.OnLose -= OnLose;
            Game.OnBotDie -= OnBotDie;
            PlayerBase.OnFire -= OnPlayerFire;
        }

        public void OnPrivacy(bool show)
        {
            privacyPanel.SetActive(show);
            menuDecor.SetActive(!show);
            titlePanel.SetActive(!show);
        }

        public void OpenPolicy()
        {
            Application.OpenURL("https://www.freeprivacypolicy.com/privacy/view/725f5fbb96d16e8e08f2d08e0c78afc6");
        }

        public void OnStart()
        {
            bool isTutor = PlayerPrefs.GetInt("TutorShown", 0) == 0;
            Game.Instance.Restart();
            gamePanel.SetActive(true);
            menuDecor.SetActive(false);
            currentScores = 0;
            scoresText.text = currentScores.ToString();
            crystalsText.text = PlayerPrefs.GetInt("crystals", 0).ToString();
            PlayerPrefs.SetInt("TutorShown", 1);
            tutorPanel.SetActive(isTutor);
            if (isTutor)
            {
                PlayerPrefs.SetInt("BoltAmount_" + BoltType.Fire.ToString(), 10);
            }
            UpdateButtons();
            healButton.interactable = PlayerPrefs.GetInt("crystals", 0) >= healCost;
            healButton.GetComponentInChildren<Text>().text = healCost.ToString();
            healProgress.gameObject.SetActive(false);
            healProgress.fillAmount = 0f;
            Time.timeScale = 1;
            if (PlayerPrefs.GetInt("castleIndex", 0) > 0)
            {
                bonusLevelText.gameObject.SetActive(true);
                bonusLevelText.DOFade(1f, 0.1f);
                bonusLevelText.DOFade(0f, 3f).OnComplete(() => { bonusLevelText.gameObject.SetActive(false); });
            }
        }

        void OnPlayerFire(BoltType boltType)
        {
            if (boltType == BoltType.Iron)
                return;
            
            int boltsAmount = PlayerPrefs.GetInt("BoltAmount_" + boltType.ToString(), 0);
            if (boltsAmount > 0)
                PlayerPrefs.SetInt("BoltAmount_" + boltType.ToString(), boltsAmount - 1);

            UpdateButtons();
        }

        void UpdateButtons()
        {
            int currArrowIndex = PlayerPrefs.GetInt("CurrentPlayerArrow", 0);
            int currAmount = 0;
            for (int i = 0; i < buttonInfos.Length; ++i)
            {
                int boltsAmount = PlayerPrefs.GetInt("BoltAmount_" + buttonInfos[i].boltType.ToString(), 0);
                buttonInfos[i].amount.text = boltsAmount.ToString();
                buttonInfos[i].button.gameObject.SetActive(boltsAmount > 0);
                if (i + 1 == currArrowIndex)
                    currAmount = boltsAmount;
            }
            if (currArrowIndex > 0)
            {
                buttonInfos[currArrowIndex - 1].button.isOn = currAmount > 0;
                if (currAmount <= 0)
                {
                    PlayerPrefs.SetInt("CurrentPlayerArrow", 0);
                    Game.Instance.ChangeArrow();
                }
            }
        }

        void OnLose()
        {
            int prevMaxScores = PlayerPrefs.GetInt("prevMaxScores", 0);
            int maxScores = PlayerPrefs.GetInt("maxScores", 0);
            loseMaxScoresText.gameObject.SetActive(currentScores > prevMaxScores);
            PlayerPrefs.SetInt("castleIndex", 0);
            if (currentScores > prevMaxScores)
            {
                AddCrystals(100);
            }
            if (prevMaxScores >= 10 && currentScores >= prevMaxScores)
                PlayerPrefs.SetInt("castleIndex", 1);

            if (PlayerPrefs.GetInt("dragonIsDead", 0) == 1)
                PlayerPrefs.SetInt("castleIndex", 2);

            PlayerPrefs.SetInt("prevMaxScores", PlayerPrefs.GetInt("maxScores", 0));
            loseScoresText.text = currentScores.ToString() + "/" + maxScores.ToString() + "(MAX)";
            Invoke("ShowLose", 1f);
        }

        void ShowLose()
        {
            losePanel.SetActive(true);
        }

        void OnBotDie(int scores, int prize)
        {
            currentScores = scores;
            scoresText.text = scores.ToString();
            AddCrystals(prize);
        }

       public  void AddCrystals(int amount)
        {
            int crystals = PlayerPrefs.GetInt("crystals", 0);
            int result = crystals + amount;
            if (result < 0)
                result = 0;
            PlayerPrefs.SetInt("crystals", result);
            crystalsText.text = result.ToString();

            if (healTime <= 0f)
                healButton.interactable = PlayerPrefs.GetInt("crystals", 0) >= healCost;
        }

        public void OnSelectArrow(int index)
        {
            PlayerPrefs.SetInt("CurrentPlayerArrow", buttonInfos[index - 1].button.isOn ? index : 0);
            Game.Instance.ChangeArrow();
        }

        public void OnPause(bool pause)
        {
            pausePanel.SetActive(pause);
            Time.timeScale = pause ? 0 : 1;
        }

        public void OnMenu()
        {
            Time.timeScale = 1f;
            Game.Instance.Stop();
            gamePanel.SetActive(false);
            pausePanel.SetActive(false);
            losePanel.SetActive(false);
            menuPanel.SetActive(true);
            menuDecor.SetActive(true);
            shopPanel.SetActive(false);
        }

        public void OnShop()
        {
            Time.timeScale = 1f;
            Game.Instance.Stop();
            gamePanel.SetActive(false);
            pausePanel.SetActive(false);
            losePanel.SetActive(false);
            menuPanel.SetActive(false);
            menuDecor.SetActive(false);
            shopPanel.SetActive(true);
        }

        void Update()
        {
            if (healTime > 0f)
            {
                healTime -= Time.deltaTime;
                healProgress.fillAmount = (healCooldown - healTime) / healCooldown;
                if (healTime <= 0f)
                {
                    healProgress.gameObject.SetActive(false);
                    healButton.interactable = PlayerPrefs.GetInt("crystals", 0) >= healCost;
                }
            }
        }

        public void OnHeal()
        {
            if (Game.Instance.HealPlayer())
            {
                AddCrystals(-healCost);
                healButton.interactable = false;
                healProgress.gameObject.SetActive(true);
                healTime = healCooldown;
                healProgress.fillAmount = 0f;
            }
        }

        public void OnExit()
        {
            Application.Quit();
        }
    }
}
