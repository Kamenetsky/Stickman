﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Stickman
{
    public class ShopItemShield : ShopItem
    {
        public ShieldType shieldType;

        [SerializeField]
        Button equipButton;

        [SerializeField]
        GameObject equipedIcon;

        public override void Refresh()
        {
            base.Refresh();

            int playerShieldIndex = PlayerPrefs.GetInt("playerShieldIndex", 0);
            bool isShieldBouth = PlayerPrefs.GetInt("isShieldBouth" + shieldType.ToString(), 0) == 1;
            bool isShieldEquiped = playerShieldIndex == (int)shieldType;
            buyButton.gameObject.SetActive(!isShieldBouth);
            equipButton.gameObject.SetActive(isShieldBouth && !isShieldEquiped);
            equipedIcon.SetActive(isShieldEquiped);
        }
    }
}
