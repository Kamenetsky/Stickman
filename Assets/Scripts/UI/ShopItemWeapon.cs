﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Stickman
{
    public class ShopItemWeapon : ShopItem
    {
        public WeaponType weaponType;

        [SerializeField]
        Button equipButton;

        [SerializeField]
        GameObject equipedIcon;

        protected override void Start()
        {
            base.Start();
        }

        public override void Refresh()
        {
            base.Refresh();

            int playerWeaponIndex = PlayerPrefs.GetInt("playerWeaponIndex", 0);
            bool isWeaponBouth = PlayerPrefs.GetInt("isWeaponBouth_" + weaponType.ToString(), 0) == 1;
            bool isWeaponEquiped = playerWeaponIndex == (int)weaponType;
            buyButton.gameObject.SetActive(!isWeaponBouth);
            equipButton.gameObject.SetActive(isWeaponBouth && !isWeaponEquiped);
            equipedIcon.SetActive(isWeaponEquiped);
        }
    }
}
