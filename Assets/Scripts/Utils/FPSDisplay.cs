﻿using UnityEngine;
using UnityEngine.UI;

public class FPSDisplay : MonoBehaviour
{
    int avgFrameRate;
    [SerializeField] Text text;

    public void Update()
    {
        float current = 0;
        current = (int)(1f / Time.unscaledDeltaTime);
        avgFrameRate = (int)current;
        text.text = avgFrameRate.ToString() + " FPS";
    }
}